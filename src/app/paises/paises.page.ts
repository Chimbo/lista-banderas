import { Component, OnInit, ViewChild } from '@angular/core';
import { IonReorderGroup } from '@ionic/angular';
import {List} from './list';
@Component({
  selector: 'app-paises',
  templateUrl: './paises.page.html',
  styleUrls: ['./paises.page.scss'],
})
export class PaisesPage implements OnInit {
  Lists: List[]
  constructor() { 
    this.Lists=[
      new List(" Afghanistan"),
      new List("African Union"),
      new List("  Aland"),
      new List("Albania  "),
      new List("Algeria"),
      new List("Andorra"),
      new List(" Angola"),
      new List("Antigua and Barbuda"),
      new List("Argentina - Long"),
      new List("Armenia"),

    ]
    
  }
  

  ngOnInit() {
  }
  @ViewChild(IonReorderGroup,{static : true})reorderGroup:IonReorderGroup;
  reordenarLista(ev: any){
    
    console.log('cambio de posicion',ev.detail.from,'to',ev.detail.to);
    ev.detail.complete();

}}

